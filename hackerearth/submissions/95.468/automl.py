
import time
start_time = time.time()


import pandas
import sys
import numpy as np
from pandas.tools.plotting import scatter_matrix
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import KFold
from sklearn.svm import SVC
import sys

filename = 'criminal_train.csv'
trainDataset = pandas.read_csv(filename,skipinitialspace=True)

filename = 'criminal_test.csv'
testDataset = pandas.read_csv(filename,skipinitialspace=True)

array = trainDataset.values
X_train = array[:,0:71]
y_train = array[:,71]
print(array.shape)
array = testDataset.values
print(array.shape)
X_test = array[:,0:71]

# from sklearn.model_selection import cross_val_predict
# from sklearn.ensemble import RandomForestClassifier
# from sklearn import svm
import autosklearn.classification
knn = autosklearn.classification.AutoSklearnClassifier()
# knn = svm.SVC()
# knn = KNeighborsClassifier(n_neighbors=5)
# knn = RandomForestClassifier(n_estimators=50,max_depth=32,n_jobs=-1)
# predictions = cross_val_predict(knn, X_train, y_train, cv=10) 
knn.fit(X_train,y_train)
predictions = knn.predict(X_test)

# from sklearn.metrics import accuracy_score
# print(accuracy_score(y_train, predictions))
# from sklearn.metrics import precision_score
# print(precision_score(y_train,predictions))
# sys.exit()


save = np.c_[X_test[:,0],predictions]
# save = np.vstack(([['PERID,Criminal']['']],save))
print('shape',X_test[:,0].shape)
print(save.shape)
np.savetxt('submission.csv',save,delimiter=',',fmt='%10.0f')
