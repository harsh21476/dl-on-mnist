import numpy as np
from sklearn.decomposition import PCA
import pandas
import matplotlib.pyplot as plt
import sys
from sklearn.neighbors import KNeighborsClassifier

filename = 'criminal_train.csv'
trainDataset = pandas.read_csv(filename,skipinitialspace=True)

filename = 'criminal_test.csv'
testDataset = pandas.read_csv(filename,skipinitialspace=True)

array = trainDataset.values
X_train = array[:,1:71]
y_train = array[:,71]
print(array.shape)
array = testDataset.values
print(array.shape)
X_test = array[:,1:71]


pca = PCA(n_components=70)
pca.fit(np.vstack((X_train,X_test)))
# print(pca.explained_variance_ratio_)  
# print(pca.singular_values_)  
X_train_new = pca.transform(X_train)

# sys.exit()

# plt.plot(pca.explained_variance_ratio_[50:])
# plt.show()

from sklearn.model_selection import cross_val_predict
from sklearn.ensemble import RandomForestClassifier
knn = KNeighborsClassifier(n_neighbors=10)
# knn = RandomForestClassifier(n_estimators=50,max_depth=32,n_jobs=-1)
predictions = cross_val_predict(knn, X_train_new, y_train, cv=10) 
# knn.fit(X_train,y_train)
# predictions = knn.predict(X_test)

from sklearn.metrics import accuracy_score
print(accuracy_score(y_train, predictions))
from sklearn.metrics import precision_score
print(precision_score(y_train,predictions))